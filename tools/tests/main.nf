include { read_images } from "$NEXTPIPES/tools/helper"

images = read_images()

process printTest {

  label 'bash'
  container { images['tests'] ?: task.container } //this needs to be commented out if docker is not installed

  output:
  stdout

  script:
  """
  echo "Congrats! You just ran a test with the following Nextflow profiles: ${workflow.profile}"
  """

}

process testFileLocalization {

  label 'bash'
  container { images['nextpipes/tests'] ?: task.container }

  input:
  path myfile

  output:
  stdout

  script:
  """
  ls -l
  echo 'Congrats! You just ran a test with the following Nextflow profiles: ${workflow.profile}.'
  echo 'Your file $myfile should be here.'
  """

}