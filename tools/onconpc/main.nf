include { read_images } from "$NEXTPIPES/tools/helper"

images = read_images()
output_dir = params.output_dir ? params.output_dir : "."

process performCUPPrediction_onconpc {

  label 'onconpc'
  publishDir "$output_dir/${task.process}/", mode : 'copy', overwrite : true
  container { images['nextpipes/onconpc'] ?: task.container }

  input:
  path 'patients.csv'
  path 'samples.csv'
  path 'variants.csv'
  path 'CNAs.csv'

  output:
  path 'features.*'
  path 'predictions.*'
  path 'shaps.*'

  script:
  """
  source activate onconpc_conda_env
  /opt/conda/envs/onconpc_conda_env/bin/python /app/onconpc/run_onconpc.py
  """

}