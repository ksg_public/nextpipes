import warnings
import numpy as np
import pandas as pd
import xgboost as xgb
import codes.utils as utils
from datetime import datetime

def sort_and_keep_top_x(row, top_x):
    # Sort the row by absolute values while retaining the original index
    sorted_row = row.reindex(row.abs().sort_values(ascending=False).index)
    # Keep top X values, set the rest to NaN
    sorted_row.iloc[top_x:] = np.NaN
    return sorted_row


def convert_df_column_types(df, source_dtype, target_dtype):
    object_columns = df.select_dtypes(include=[source_dtype]).columns
    df[object_columns] = df[object_columns].astype(target_dtype)

    return df

# Load mock data with three tumor samples
patients_df = pd.read_csv('patients.csv')
samples_df = pd.read_csv('samples.csv')
mutations_df = pd.read_csv('variants.csv')
mutations_df = mutations_df[~mutations_df['Hugo_Symbol'].isna()]
cna_df = pd.read_csv('CNAs.csv')

# Get features and labels for OncoNPC predictive inference
df_features, df_labels = utils.get_onconpc_features_from_raw_data(
    patients_df,
    samples_df,
    mutations_df,
    cna_df,
    features_onconpc_path='/app/onconpc/data/features_onconpc.pkl',
    combined_cohort_age_stats_path='/app/onconpc/data/combined_cohort_age_stats.pkl',
    mut_sig_weights_filepath='/app/onconpc/data/mutation_signatures/sigProfiler*.csv'
)

df_features_out = df_features.copy()
df_features_out.index.name = 'SAMPLE_ID'
df_features_out.reset_index(inplace=True)
df_features_out.to_csv("features.csv", index=False)
df_features_out.to_pickle("features.pickle")

# Load fully trained OncoNPC model
xgb_onconpc = xgb.Booster()
xgb_onconpc.load_model('/app/onconpc/model/xgboost_v1.7.6_OncoNPC_full.json')

# Specify cancer types to consider
cancer_types_to_consider = ['Acute Myeloid Leukemia', 'Bladder Urothelial Carcinoma', 'Cholangiocarcinoma',
                            'Colorectal Adenocarcinoma', 'Diffuse Glioma', 'Endometrial Carcinoma',
                            'Esophagogastric Adenocarcinoma', 'Gastrointestinal Neuroendocrine Tumors',
                            'Gastrointestinal Stromal Tumor',
                            'Head and Neck Squamous Cell Carcinoma', 'Invasive Breast Carcinoma', 'Melanoma',
                            'Meningothelial Tumor',
                            'Non-Hodgkin Lymphoma', 'Non-Small Cell Lung Cancer', 'Ovarian Epithelial Tumor',
                            'Pancreatic Adenocarcinoma',
                            'Pancreatic Neuroendocrine Tumor', 'Pleural Mesothelioma', 'Prostate Adenocarcinoma',
                            'Renal Cell Carcinoma',
                            'Well-Differentiated Thyroid Cancer']

# Predict primary sites of CUP tumors
preds_df = utils.get_xgboost_latest_cancer_type_preds(xgb_onconpc,
                                                      df_features,
                                                      cancer_types_to_consider)

preds_df.index.name = 'SAMPLE_ID'
preds_df_out = preds_df.copy()
preds_df_out.reset_index(inplace=True)
preds_df_out.to_csv("predictions.csv", index=False)
preds_df_out.to_pickle("predictions.pickle")

# Get SHAP values
warnings.filterwarnings('ignore')
shaps = utils.obtain_shap_values_with_latest_xgboost(xgb_onconpc, df_features)
shaps_df = pd.DataFrame(np.concatenate(shaps), columns=df_features.columns)
shaps_df['cancer_type'] = np.repeat(cancer_types_to_consider, len(df_features))
shaps_df['SAMPLE_ID'] = np.tile(preds_df.index, len(cancer_types_to_consider))

shaps_df.set_index(['SAMPLE_ID', 'cancer_type'], inplace=True)

shaps_df.reset_index().to_csv("shaps.csv", index=False)

# Custom shaps structure
print("Keeping top 25 values")
shaps_df2 = shaps_df.apply(sort_and_keep_top_x, axis=1, top_x=25)

# Convert DataFrame columns to sparse
print("Converting shaps to sparse")
dtype = pd.SparseDtype(float)
for c in shaps_df2.columns:
    shaps_df2[c] = shaps_df2[c].astype(dtype)

shaps_df2 = shaps_df2.melt(ignore_index=False)
shaps_df2 = shaps_df2.dropna()
shaps_df2 = shaps_df2.reset_index()
shaps_df2['SAMPLE_ID'] = shaps_df2['SAMPLE_ID'].astype('category')
shaps_df2['cancer_type'] = shaps_df2['cancer_type'].astype('category')
shaps_df2['variable'] = shaps_df2['variable'].astype('category')

shaps_df2 = shaps_df2.set_index(['SAMPLE_ID', 'cancer_type'])
shaps_df2 = shaps_df2.sort_index()

print("Writing shaps pickle")
shaps_df2.to_pickle("shaps.pickle")
