FROM continuumio/miniconda3

WORKDIR /app/

# Clone the GitHub repository
RUN git clone https://github.com/itmoon7/onconpc.git \
    && cd onconpc \
    && git checkout 352412507d3c6a8b9c4df19932c8adb22eaceeb9

# Change to the cloned repository directory
WORKDIR /app/onconpc

# Create Conda environment from YAML file
RUN conda env create -f onconpc_conda.yml && conda clean --all --yes

# Activate the Conda environment
RUN echo "conda activate onconpc_conda_env" >> ~/.bashrc
SHELL ["/bin/bash", "--login", "-c"]

# Make the R package installation script executable
RUN chmod +x install_r_packages_onconpc.sh && ./install_r_packages_onconpc.sh

RUN conda install pandas=2.2.1

COPY . /app/onconpc

ENV PYTHONPATH "${PYTHONPATH}:/app/onconpc"

LABEL maintainer="mdeletto@ds.dfci.harvard.edu"
LABEL org.opencontainers.image.title="onconpc"
LABEL org.opencontainers.image.description="OncoNPC w/ Conda & R dependencies"
LABEL org.opencontainers.image.version="1.0"
LABEL org.opencontainers.image.authors="mdeletto@ds.dfci.harvard.edu"
