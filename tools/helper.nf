package nextflow.file
import groovy.json.JsonSlurper

params.images_json = "$NEXTPIPES/images.json"

def read_images() {
    return new JsonSlurper().parse(new File(params.images_json))
}

def createChannelFromCsv(csvFile) {
    sample_run_ch = Channel.fromPath(csvFile) \
        | splitCsv(header: true) \
        | map { row ->
            tuple(
                FileHelper.asPath("gs://${row.briefcase}/${row.cbio_sample_id}/aligned/${row.cbio_sample_id}.dedup.cleaned.bam"),
                FileHelper.asPath("gs://${row.briefcase}/${row.cbio_sample_id}/aligned/${row.cbio_sample_id}.dedup.cleaned.bai")
            )
        }
    return sample_run_ch
}