params.gatkPath = "/gatk/gatk.jar"
include { read_images } from "$NEXTPIPES/tools/helper"
images = read_images()
output_dir = params.output_dir ? params.output_dir : "."


process apply_bqsr {
    container { images['nextpipes/gatk'] ?: task.container }
    cpus 4
    memory '8 GB'

    input:
    tuple val(bam_base_name), path(bam), path(bam_index), path(recal_data)
    path ref_fasta
    path ref_fasta_index
    path ref_fasta_dict

    output:
    tuple path("${bam.baseName}.bqsr.bam"), path("${bam.baseName}.bqsr.bam.bai"), emit: bqsr_tuple

    script:
    """
    java -Xmx8G -Xms8G -jar ${params.gatkPath} ApplyBQSR -I ${bam} -R ${ref_fasta} --bqsr-recal-file ${recal_data} -O ${bam.baseName}.bqsr.bam
    samtools index ${bam.baseName}.bqsr.bam
    """
}


process joint_genotyping {
    cpus 4
    memory '8 GB'
    publishDir "$output_dir/${task.process}/", mode : 'copy', overwrite : true
    container { images['nextpipes/gatk'] ?: task.container }

    input:
    path gvcf
    path ref_fasta
    path ref_fasta_index
    path ref_fasta_dict

    output:
    path "${gvcf.baseName}.final.vcf", emit: final_vcf

    script:
    """
    java -Xmx8G -Xms8G -jar ${params.gatkPath} GenotypeGVCFs -V ${gvcf} -R ${ref_fasta} -O ${gvcf.baseName}.final.vcf
    """
}

process haplotype_calling {
    container { images['nextpipes/gatk'] ?: task.container }
    cpus 4
    memory '8 GB'
    input:
    tuple path(bam), path(bam_index)
    path ref_fasta
    path ref_fasta_index
    path ref_fasta_dict

    output:
    path "${bam.baseName}.g.vcf", emit: gvcf_files

    script:
    """
    java -Xmx8G -Xms8G -jar ${params.gatkPath} HaplotypeCaller -I ${bam} -R ${ref_fasta} -O ${bam.baseName}.g.vcf -ERC GVCF
    """
}

process base_recalibration {
    container { images['nextpipes/gatk'] ?: task.container }
    cpus 4
    memory '8 GB'

    input:
    tuple path(bam), path(bam_index)
    path known_sites
    path known_sites_index
    path ref_fasta
    path ref_fasta_index
    path ref_fasta_dict

    output:
    tuple val("${bam.baseName}"), path("${bam.baseName}.out.bam"), path("${bam.baseName}.out.bai"), path("${bam.baseName}.recal_data.table"), emit: bam_recal_tuple

    script:
    """
    java -Xmx8G -Xms8G -jar ${params.gatkPath} BaseRecalibrator -I ${bam} -R ${ref_fasta} --known-sites ${known_sites} -O ${bam.baseName}.recal_data.table
    cp ${bam} ${bam.baseName}.out.bam
    cp ${bam_index} ${bam.baseName}.out.bai
    """
}
