#!/usr/bin/env bash

# shellcheck source=./../../incl.sh
. "$(dirname "$(readlink -f "$0")")"/../incl.sh

set -o errexit
set -o nounset
set -o pipefail

# First arg will be images.json, assign contents to images_json

image="$1"

push_result="$(docker push "$image")"


# Extract sha256 from last line of output e.g.
# 2.0.2909: digest: sha256:2cddac8f83d5546d06548d8712ac4f819b956c313e390529dd10b004752329e4 size: 3050
sha_hash="$(echo "$push_result" | awk '/digest: / {print $3}')"

# If no hash, bail out
if test -z "$sha_hash"; then
  log error "$(printf "ERROR: Unable to determine hash for %s\n" "$image")"
  exit 1
fi

# Feature request: https://gitlab.partners.org/ksg_bwh/vega/tools/-/issues/28
# Add check to evaluate local image post-push and determine that sha from push is in the repo digest(s)
inspect_result="$(docker image inspect --format "{{.RepoDigests}})" $image )"

if ! grep -q "$sha_hash" <<< "${inspect_result}"; then
  log error "$(printf "SHA returned from docker push not in image inspect: %s" "$image" )"
  exit 1
fi

# Extract the image name with url from first line of push e.g.
# The push refers to repository [docker-bcb.dfci.harvard.edu/vega/mutect2-post]
# Result should be docker-bcb.dfci.harvard.edu/vega/mutect2-post
fq_image_name=$(awk -F"[][]" '{print $2}' <<<"$push_result" | tr -d '[:space:]')

# Take image name and extract last 'part' after slash, which results in the tool name e.g.
# docker-bcb.dfci.harvard.edu/vega/mutect2-post becomes mutect2-post
tool_name=$(awk -F/ '{print $NF}' <<<"$fq_image_name")

# With the tool name and new hash, we can access images json and write the new hash to the right key
target_value=$(printf "%s@%s" "$fq_image_name" "$sha_hash")
json="{ \"nextpipes/$tool_name\": \"$target_value\" }"

echo $json
