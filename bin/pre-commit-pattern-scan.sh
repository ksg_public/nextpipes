#!/bin/bash

# Define the pattern file and exclusion pattern
PATTERNS_FILE="etc/remove_patterns.txt"
EXCLUDE_PATTERN="(\.bam)$"
OFFENDING_FILES=0

# Ensure the patterns file exists
if [ ! -f "$PATTERNS_FILE" ]; then
  echo "ERROR: Patterns file '$PATTERNS_FILE' not found."
  exit 1
fi

# Iterate over each pattern in the patterns file
while IFS= read -r PATTERN; do
  # Check each staged file against the current pattern
  for FILE in $(git diff --cached --name-only | grep -vE "$EXCLUDE_PATTERN"); do
    if grep -q -E "$PATTERN" "$FILE"; then
      echo "ERROR: Pattern '$PATTERN' found in file: $FILE"
      OFFENDING_FILES=1
    fi
  done
done < "$PATTERNS_FILE"

# Exit with error if any offending files are found
if [ $OFFENDING_FILES -eq 1 ]; then
  echo "Commit rejected. Please fix the detected issues."
  exit 1
fi

echo "No disallowed patterns found. Commit allowed."
exit 0
