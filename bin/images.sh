#!/usr/bin/env bash

function test_image_manifest() {
  local __manifest_str="$1"
  local __replace_str="$2"

  # Detect if the replacement is an @sha256 string. If not, add : prefix
  if ! [[ "$__replace_str" =~ ^\@sha256.* ]]; then
    __replace_str=":$__replace_str"
  fi

  echo "$__manifest_str" | sed "s/\@sha256.*\"/\\$__replace_str\"/g"
}

function get_image_tag() {
  local __manifest_str="$1"
  local __key="$2"
  local tag_result

  tag_result="$(jq --arg IMG_KEY "$__key" '.[$IMG_KEY]' <<<"$__manifest_str")"

  echo "$tag_result"
}

function sha_from_docker_push(){
  local push_result="$1"

  echo "$push_result" | awk '/digest: / {print $3}'
}

function image_name_from_docker_push(){
  local push_result="$1"

  awk -F"[][]" '{print $2}' <<<"$push_result" | tr -d '[:space:]'
}

function tool_name_from_image_name(){
  local image_name="$1"

  #TODO: compensate for image names with : or @ tags
  cut -d/ -f 3 <<< "$image_name"
}