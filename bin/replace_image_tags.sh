#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <json_file> <key> <value>"
    exit 1
fi

# Assigning arguments to variables
json_file="$1"
key="$2"
value="$3"

# Check if the JSON file exists
if [ ! -f "$json_file" ]; then
    echo "Error: JSON file '$json_file' not found."
    exit 1
fi

# Modify the JSON using jq
jq --arg key "$key" --arg value "$value" '. + { ($key): $value }' "$json_file" > temp.json && mv temp.json "$json_file"
