#!/usr/bin/env bash

# shellcheck source=./../../incl.sh
. "$(dirname "$(readlink -f "$0")")"/images.sh

set -o errexit
set -o nounset
set -o pipefail

usage() {
  echo "Usage: write_test_manifest EXISTING_FILE NEW_FILE [TAG]"
  echo "From a json of images, write a new file with new tags"
  echo "DESCRIPTION"
  echo "From an existing manifest, write a new manifest with a specified key or key defined in VERSION_NUMBER" | fold | awk '{ print "\t" $0 }'
  echo "INPUTS"
  printf "\t%10s %20s\n" "EXISTING_FILE" "Location of images JSON file"
  printf "\t%10s %20s\n" "NEW_FILE" "Location to write new images JSON file"
  printf "\t%10s %20s\n" "TAG" "New tag or sha256 to replace tags/shas"
  echo ""
}

if [ -z "${1-}" ] || [ -z "${2-}" ]; then
  usage
  exit 1
fi

if test ! -f "$1"; then
  printf "ERROR: File: %s not found\n" "$1"
  usage
  exit 1
fi

from_file="$1"
to_file="$2"

# Detect if no tag passed and try to resolve from VERSION_NUMBER provided in CI
if test -z "${3-}"; then
  if test -z "${VERSION_NUMBER-}"; then
    echo "ERROR: No tag passed and VERSION_NUMBER env var not defined"
    usage
    exit 1
  else
    tag="${VERSION_NUMBER}"
  fi
else
  tag="$3"
fi

contents="$(cat "$from_file")"

new_manifest="$(test_image_manifest "$contents" "$tag")"
TEMP_FILE=$(mktemp)
echo "{" >> "$TEMP_FILE"
jq -r 'to_entries[] | "\(.key) \(.value)"' <<< "$new_manifest" | while read -r key value; do
    # Split the value by '/'
    first_part=$(echo "$value" | awk -F '/nextpipes/' '{print $1}')
    rest=$(echo "$value" | awk -F '/nextpipes/' '{print $2}')


    # Replace the first part with the environment value
    new_value="$REGISTRY_URL/nextpipes/$rest"

    # Update the JSON-like string with the new value
    echo "\"$key\": \"$new_value\"," >> "$TEMP_FILE"
done
echo "}" >> "$TEMP_FILE"
sed -zr 's/,([^,]*$)/\1/' "$TEMP_FILE" > "$to_file"
