#!/bin/bash

# Get exact location of this script and then walk back to root
PRJ_HOST_ROOT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Build on project root to utils dir
BIN_DIR="${PRJ_HOST_ROOT_DIR}"/bin
readonly BIN_DIR

. "${BIN_DIR}"/log.sh

. "${BIN_DIR}"/images.sh

# Provides extremely accurate abs path of caller script
# Use instead of PWD
# One short-coming of this approach is that a 'caller' like bats or other testing suites
# for bash will cause 'this dir' to most likely be their lib path and completely change expected
# behavior. For safety, it is recommended to use path relative to PRJ_HOST_ROOT_DIR where possible
if [[ -n "${THIS_DIR-}" ]]; then
  THIS_DIR="${THIS_DIR}"
else
  THIS_DIR=$(dirname "$(readlink -f "$0")")
fi
readonly THIS_DIR

# Provide reliable path to refs directory
if [[ -n "${REFS_DIR-}" ]]; then
  REFS_DIR="${REFS_DIR}"
else
  # By default, assuming vega/reference git LFS project has been checked out at /data/gitlab/reference:
  REFS_DIR=/data/gitlab/reference
fi


readonly REFS_DIR

# Vars set here due to frequent usage in test scripts.
TMP_ROOT="${THIS_DIR}"/.tmp
readonly TMP_ROOT

OUTPUT_FOLDER="${TMP_ROOT}"/output
readonly OUTPUT_FOLDER

TMP_CROMWELL_EXEC_FOLDER="${TMP_ROOT}"/cromwell-executions
readonly TMP_CROMWELL_EXEC_FOLDER
