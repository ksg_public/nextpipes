# Nextpipes
  
The Nextpipes project is maintained by the Knowledge Systems Group. [Nextpipes](https://gitlab.com/ksg_public/nextpipes) is designed to provide
a basic framework for writing and executing modularized bioinformatics workflows using the Nextflow workflow engine. It is suggested that users familiarize themselves with the [Nextflow](https://www.nextflow.io/docs/latest/index.html) framework before writing their own workflows.

The project makes use of Nextflow's ability to generate configuration [profiles](https://www.nextflow.io/docs/latest/config.html#config-profiles),
which enables us to provide a number of custom configurations for different execution environments across the DFCI and Google Cloud ecosystems.
``
It is highly suggested that you use some sort of containerization technology to run your workflows. Otherwise, you will need to ensure each task called by a workflow has all tool binaries in the user's `$PATH`. Supported containerization technologies can be found in Nextflow's documentation. We have also included profiles for some of the more popular technologies (see @tbl-nextpipes-containers).

Installation of these containerization technologies is outside the scope of this guide; users should consult the official documentation for the selected technology.
``
# Getting Started

1. Clone the repository.

    ```bash
    git clone $URL
    cd nextpipes
    ```

2. Install the Java SDK [>= 11, <= 18](https://www.nextflow.io/docs/latest/getstarted.html#requirements). 
For example, on [Ubuntu/Debian](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04)-based systems:

    ```bash
    sudo apt update && apt install default-jdk
    ```

3. The repository comes with a committed Nextflow binary. However, should you choose to install Nextflow yourself, you can do so with:

    ```bash
    ./install.sh
    ```

4. Source the .env so that Nextpipes has access to environment variables.

    ```bash
    set -a
    source .env
    set +a
    ```

5. Run a test using some profiles (e.g. `local,docker`). 
The `test.nf` workflow below will run two tasks. 
The first task `printTest` will print all profiles executed under that pipeline run. 
The second task `testFileLocalization` will localize a file and list its parent directory, which can be helpful for assessing whether a file was mounted within a task container successfully.

    ```bash
    $ nextflow run $NEXTPIPES/workflows/test.nf -profile local,docker --myfile /path/to/README.html
    
    Nextflow 23.10.1 is available - Please consider updating your version to it
    N E X T F L O W  ~  version 23.11.0-edge
    Launching `workflows/test.nf` [cheesy_carson] DSL2 - revision: 2a3fffe2b5
    executor >  local (2)
    [c4/02c3db] process > printTest            [100%] 1 of 1 ✔
    [c5/298836] process > testFileLocalization [100%] 1 of 1 ✔
    Congrats! You just ran a test with the following Nextflow profiles: local,docker
    
    total 0
    lrwxrwxrwx    1 root     root            40 Apr 16 19:42 README.html -> /path/to/README.html
    Congrats! You just ran a test with the following Nextflow profiles: local,docker.
    Your file README.html should be here.
    
    Completed at: 16-Apr-2024 15:43:11
    Duration    : 2m 54s
    CPU hours   : (a few seconds)
    Succeeded   : 2
    ```
***
## Profiles

Configuration files can contain the definition of one or more profiles. A profile is a set of configuration attributes that can be selected during pipeline execution by using the `-profile` command line option.

One or more profiles can be enabled by separating them with commas (e.g. `-profile local,docker`). The order of the included profiles is important; profiles defined later in this list may override settings from those referenced earlier in the list.


### Profile Inclusion

Within the Nextpipes project, profiles are included by referencing their source configuration files in the `nextflow.config` file using the `includeConfig` function. Nextflow looks for the `nextflow.config` file in the project's base directory, but it can also be specified with the `-c /path/to/nextflow.config` argument. Nextflow's priority for the order of settings is described [here](https://www.nextflow.io/docs/latest/config.html#configuration-file).

As an example, below are the contents of the `nextflow.config` file included in the Nextpipes project:

```bash
// Define the default nextflow working directory
workDir = "./work"
params.output_dir = params.output_dir ?: "."

// IMPORTANT:
// It is generally suggested to only import configurations you need in your nextflow.config
// This is because some configurations rely on specific parameters,
// and will fail if not defined (e.g. params.gcp_location)
// To include more configs, follow the format below:

// Container configurations
includeConfig "./conf/containers/nextflow.conda.config"
includeConfig "./conf/containers/nextflow.docker.config"
includeConfig "./conf/containers/nextflow.singularity.config"

// Executor configurations
includeConfig "./conf/executors/nextflow.local.config"
includeConfig "./conf/executors/nextflow.google_batch.config"

// General configurations
includeConfig "./conf/general/nextflow.email.config"
includeConfig "./conf/general/nextflow.global.config"

// Reference & Resource configurations
includeConfig "./conf/references/nextflow.references.config"
includeConfig "./conf/resources/nextflow.resources.config"
```

### Available Profiles 

The tables below detail the available profiles included in the Nextpipes project. These are provided as a convenience and are intended as examples. For example, the more generic profiles (e.g. `-profile slurm`) have not been optimized and only enable basic task execution in that environment using default settings. Users are encouraged to add additional parameters to the provided profiles to suit the specific use cases and computational requirements of their workflows.  

### Containerization

| Profile | Description                       |
|---------|-----------------------------------|
| `docker`  | Enable process execution in Docker container |
| `singularity`  | Enable process execution in Singularity container |
| `conda`  | Enable process execution via Conda environment   |

### Execution

| Profile | Description                       |
|---------|-----------------------------------|
| `local`  | Enable process execution with local executor (DEFAULT) |
| `lsf`  | Enable process execution with LSF job scheduler |
| `slurm`  | Enable process execution with SLURM job scheduler |
| `google_batch`  | Enable process execution with Google Batch on Google Cloud Platform (GCP) |

### Clusters

| Profile                     | Description                                                                                                                                                |
|-----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `eris2_{short,normal,long}` | Execution profile for ERISTwo HPC using SLURM job scheduler. `short`,`normal`, and `long` in this context refers to the specific SLURM partition for jobs. |
| `kraken`                    | Execution profile for Kraken HPC using SLURM job scheduler.                                                                                                |

### Resources

| Profile | Description                       |
|---------|-----------------------------------|
| `std_resources`  | Example execution profile for setting static resources on a per task basis |
| `dynamic_memory`   | Example execution profile for setting dynamic resources on a per task basis |

# DevOps

### PHI Pattern Checking

This repository has a 2-fold method for detecting PHI-specific patterns within files. 

The first method is a pre-commit hook under `bin/pre-commit-pattern-scan.sh`, which will prevent files that contain specific patterns from being committed to the repository.
This method relies on the `pre-commit` python package. To implement this hook, perform the following steps:

```bash
# Install the requirements.txt
pip install -r /path/to/requirements.txt
# Install the pre-commit hook
pre-commit install
# Optionally, run the hook against all files in the project to assess if any disallowed patterns exist in your files. 
pre-commit run --all-files
```

The second method runs a job within a Gitlab CI pipeline called `phi_pattern_check`.
This job will search for all patterns contained within `etc/remove_patterns.txt` and will fail if detected within file contents.
Exclusion patterns for filenames can also be defined.