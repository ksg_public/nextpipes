# General

***

Configuration files in the `general` section provide helpful settings for workflow introspection, alerting, and monitoring.

## Global

These settings enable report and tracing files for monitoring resources, job status, etc. 
Note that these settings are enabled by default and are not associated with any particular profile.

## Email

The `email` profile allows for email notifications of workflow completion, and provides the same HTML report from the settings above.