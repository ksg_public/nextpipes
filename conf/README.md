# Conda environment set up for ERIS

Miniconda installation for eris2 could be done by running the following commands
```
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
~/miniconda3/bin/conda init bash
source ~/.bashrc
```
After this you should be bale to use conda profile while running nextpipes

# Installation for kraken

Jave in Kraken doesn't meet java requirements of the nextflow so java needs to be installed through the conda. It can be installed by the following commands
```
mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
~/miniconda3/bin/conda init bash
source ~/.bashrc
./bin/conda create --name nextpipes python=3.6
conda activate nextpipes
conda install -c conda-forge openjdk=11
```
Do not forget to activate nextpipes profile otherwise it will be using the default java whihc is not compatible with nextflow. If you dont want to activate each time you can add following line to your ~/.bashrc and whenver you log in it will automatically activate the conda environment. You can use nano to edit the file.
```
nano ~/.bashrc
```
add this line in the bottom of the file
```
conda activate nextflow
```
You can close and save by using ctrl+x and press y to save.

You can now use the nextflow binary in git repo. To make sure that nextpipes installed successfully you can run
```
nextflow run hello --output_dir ./
```

# Singularity for ERIS2

To use singularity you would first need to load singularity module. So you need to include following command in your sbatch script to be able to use it.

```
module load singularity/3.7.0
```


# Example sbatch script

Since kraken and eris2 is running by using slurm job submission system tasks need to be run through submitting jobs using sbatch like the following

```
sbatch /path/to/script.sbatch
```

There are a few example sbatch scripts that you can use as a template

## ERIS Singularity example

```
#!/bin/bash

#SBATCH --job-name=your_job_name
#SBATCH --partition=short
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4G
#SBATCH --output=log_prefix_%J.out
#SBATCH --error=log_prefix_%J.err

module load nextflow/23.04.3.5875
module load singularity/3.7.0

export NEXTPIPES=/path/to/nextpipes/

nextflow run ./workflows/test.nf -profile eris2,singularity -c nextflow.config --myfile /home/gufran/1.txt
```

## ERIS Conda example

```
#!/bin/bash

#SBATCH --job-name=your_job_name
#SBATCH --partition=short
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4G
#SBATCH --output=log_prefix_%J.out
#SBATCH --error=log_prefix_%J.err

module load nextflow/23.04.3.5875

export NEXTPIPES=/path/to/nextpipes/

nextflow run ./workflows/test.nf -profile eris2,conda -c nextflow.config --myfile /home/gufran/1.txt
```