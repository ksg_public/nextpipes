# Containerization
***
Nextflow supports a variety of container runtimes. 
Containerization allows you to write self-contained and truly reproducible computational pipelines, 
by packaging the binary dependencies of a script into a standard and portable format that can be executed 
on any platform that supports a container runtime. Furthermore, the same pipeline can be transparently executed 
with any of the supported container runtimes, depending on which runtimes are available in the target compute environment.

This repository contains several helpful containerization profiles to help you get started:

- Docker
- Singularity
- Conda

To use any of these containerization technologies, you must have the appropriate dependencies installed 
(e.g. Docker must be installed to use the `docker` profile).

More information regarding Nextflow's containerization support can be found [here](https://www.nextflow.io/docs/latest/container.html).
