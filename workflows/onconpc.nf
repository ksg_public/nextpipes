#!/usr/bin/env nextflow

def helpMessage() {
    log.info"""
    ===================================
     test.nf --options
    ===================================
    Description:

    This workflow runs the OncoNPC tool for cancer of unknown primary (CUP) predictions.
    See more here (https://github.com/itmoon7/onconpc).

    Usage:

    nextflow run onconpc.nf -profile docker,local \
        --patients_csv '/path/to/patients.csv' \
        --samples_csv '/path/to/samples.csv' \
        --variants_csv '/path/to/variants.csv' \
        --cnvs_csv '/path/to/cnvs.csv'

    Mandatory arguments:
      -profile             List of configuration profiles, comma-separated.

    Other options:
      --output_dir           The output directory where the results will be saved. Default is current working directory.

    """.stripIndent()
}

// Show help message
params.help = false
if (params.help){
    helpMessage()
    exit 0
}

// ARGUMENTS
output_dir = params.output_dir ? params.output_dir : "."

include { performCUPPrediction_onconpc } from "$NEXTPIPES/tools/onconpc"

workflow {

  // Set the output dir
  output_dir = output_dir

  // Run OncoNPC
  performCUPPrediction_onconpc(
    params.patients_csv,
    params.samples_csv,
    params.variants_csv,
    params.cnvs_csv
  )

}
