#!/usr/bin/env nextflow

def helpMessage() {
    log.info"""
    ===================================
     test.nf --options
    ===================================
    Description:

    This is a basic test workflow.

    Usage:

    nextflow run test.nf -profile docker,local,grch37,std_resources --myfile '/path/to/myfile.txt'

    Mandatory arguments:
      -profile             List of configuration profiles, comma-separated.

    Other options:
      --output_dir           The output directory where the results will be saved. Default is current working directory.

    """.stripIndent()
}

// Show help message
params.help = false
if (params.help){
    helpMessage()
    exit 0
}

// ARGUMENTS
output_dir = params.output_dir ? params.output_dir : "."

// import modules
include { read_images } from "$NEXTPIPES/tools/helper"

images = read_images()

include { printTest; testFileLocalization } from "$NEXTPIPES/tools/tests"

workflow {

  printTest | view
  testFileLocalization(params.myfile) | view
}
