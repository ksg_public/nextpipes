package nextflow.file

def helpMessage() {
    log.info"""
    ===================================
     variant_calling_from_bam.nf --options
    ===================================
    Description:

    This is an example bioinformatics workflow which will use a dedupped bam file (since this is the file that will be shared through the bam transfer requests).

    Usage:

    nextflow run workflows/variant_calling_from_bam.nf -c nextflow.config -profile google_batch,gcp_grch37 --csvFile /path/to/transferred_samples.csv

    Mandatory arguments:
      -profile             List of configuration profiles, comma-separated. google_batch is required for running in gcp executor and gcp_grch37 is used to locate the reference files that are going to be used in the pipeline. These files could be swapped out if you desire to use your own reference files. To swap them edit nextflow.references.config under references folder which is under conf directory.
      --csvFile            Path to the csv file this will be the transferred_samples.csv file that is shared throught the bam transfer requests. This file could be imitated as long as file has cbio_sample_id and briefcase column which is required for pinpointing samples

    Other options:
      --output_dir           The output directory where the results will be saved. Default is current working directory.

    """.stripIndent()
}

// Show help message
params.help = false
if (params.help){
    helpMessage()
    exit 0
}

params.gatkPath = "/gatk/gatk.jar"
output_dir = params.output_dir ? params.output_dir : "."

include { base_recalibration } from "$NEXTPIPES/tools/gatk"
include { apply_bqsr } from "$NEXTPIPES/tools/gatk"
include { haplotype_calling } from "$NEXTPIPES/tools/gatk"
include { joint_genotyping } from "$NEXTPIPES/tools/gatk"
include { createChannelFromCsv } from "$NEXTPIPES/tools/helper"

sample_run_ch = createChannelFromCsv(params.csvFile)


// Define pipeline structure
workflow {
    output_dir = output_dir
    base_recalibration(bam=sample_run_ch, known_sites=FileHelper.asPath(params.known_sites), known_sites_index=FileHelper.asPath(params.known_sites_index), ref_fasta=FileHelper.asPath(params.ref_fasta), ref_fasta_index=FileHelper.asPath(params.ref_fasta_index), ref_fasta_dict=FileHelper.asPath(params.ref_fasta_dict))

    apply_bqsr(base_recalibration.out.bam_recal_tuple, ref_fasta=FileHelper.asPath(params.ref_fasta), ref_fasta_index=FileHelper.asPath(params.ref_fasta_index), ref_fasta_dict=FileHelper.asPath(params.ref_fasta_dict))

    haplotype_calling(apply_bqsr.out.bqsr_tuple, ref_fasta=FileHelper.asPath(params.ref_fasta), ref_fasta_index=FileHelper.asPath(params.ref_fasta_index), ref_fasta_dict=FileHelper.asPath(params.ref_fasta_dict))

    joint_genotyping(gvcf=haplotype_calling.out.gvcf_files, ref_fasta=FileHelper.asPath(params.ref_fasta), ref_fasta_index=FileHelper.asPath(params.ref_fasta_index), ref_fasta_dict=FileHelper.asPath(params.ref_fasta_dict))

}
